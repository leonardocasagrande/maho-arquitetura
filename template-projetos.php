<?php
        $contador = 1;
        $img_id = 1;
        $argsPortfolio = array(
          'post_type' => 'portfolio',
          'order' => 'ASC',
          'posts_per_page' => -1
        );
        $portfolio = new WP_Query($argsPortfolio);

        if ($portfolio->have_posts()) : while ($portfolio->have_posts()) : $portfolio->the_post();
        ?> <?php
            $images = get_field('galeria');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)
            if ($images) : ?> <div class="item"> <?php foreach ($images as $image) : ?> <?php if ($contador == 1) : ?> <a class="col-md-5 px-0" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="height:220px; background: url( <?php echo esc_url($image['sizes']['medium']) ?>);"><!-- <img src="<?php echo esc_url($image['sizes']['medium']) ?>" alt="" class="img-fluid"> --><div class="efeito-hover"><div class="filtro"></div><span class="plus">+</span></div></div></a> <?php else : ?> <a class="d-none" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="height:220px; background: url( <?php echo esc_url($image['sizes']['medium']) ?>);"><div class="efeito-hover"><div class="filtro"></div><span class="plus">+</span></div></div></a> <?php endif;
                  $contador++;
                endforeach;
                $contador = 1; ?> </div> <?php endif; ?> <?php
          endwhile;
        endif; wp_reset_query();?>
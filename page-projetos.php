<?php get_header(); ?> <section class="page-projetos d-lg-none"><div class="cabecalho"><h1 class="titulo">projetos</h1><p>Para o seu projeto residencial, comercial ou corporativo, nós oferecemos atendimento completo: arquitetura, design de interiores e paisagismo, para um resultado personalizado e harmônico.</p><ul class="nav nav-tabs" id="myTab" role="tablist"><li class="nav-item" role="presentation"><a class="nav-link active" id="arquitetura-tab" data-toggle="tab" href="#arquitetura" role="tab" aria-controls="arquitetura" aria-selected="true">arquitetura</a></li><li class="nav-item" role="presentation"><a class="nav-link" id="design-tab" data-toggle="tab" href="#design" role="tab" aria-controls="design" aria-selected="false">design de interiores</a></li><li class="nav-item" role="presentation"><a class="nav-link" id="paisagismo-tab" data-toggle="tab" href="#paisagismo" role="tab" aria-controls="paisagismo" aria-selected="false">paisagismo</a></li></ul></div><div class="tab-content" id="myTabContent"><div class="tab-pane portfolios fade show active" id="arquitetura" role="tabpanel" aria-labelledby="arquitetura-tab"> <?php
        $contador = 1;
        $contadorItems = 1;
        $argsPortfolioArq = array(
          'post_type' => 'portfolio',
          'order' => 'ASC',
          'tax_query' => array(
              array(
                'taxonomy' => 'categorias_portfolio',
                'field' => 'slug',
                'terms' => 'arquitetura'
                
              )
          ),
          'posts_per_page' => -1
        );
        $portfolioArq = new WP_Query($argsPortfolioArq);

        if ($portfolioArq->have_posts()) : while ($portfolioArq->have_posts()) : $portfolioArq->the_post();
        ?> <?php
            $images = get_field('fotos');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)
            if ($images) : ?> <div class="item i--<?= $contadorItems ?>"> <?php foreach ($images as $image) : ?> <?php if ($contador == 1) : ?> <a class="px-0" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"><!-- <img src="<?php echo esc_url($image['sizes']['large']) ?>" alt="" class="img-fluid"> --></div></a> <?php else : ?> <a class="d-none" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"></div></a> <?php endif;
                  $contador++;
                endforeach;
                $contador = 1; ?> </div> <?php endif; ?> <?php
        $contadorItems++;
        if($contadorItems > 5){
            $contadorItems = 1;
        }
          endwhile;
        endif; wp_reset_query();?> <!-- arquitetura --></div><div class="tab-pane portfolios fade" id="design" role="tabpanel" aria-labelledby="design-tab"> <?php
        $contador = 1;
        $contadorItems = 1;
        $argsPortfolioDes = array(
          'post_type' => 'portfolio',
          'order' => 'ASC',
          'tax_query' => array(
            array(
              'taxonomy' => 'categorias_portfolio',
              'field' => 'slug',
              'terms' => 'design-de-interiores'
              
            )
        ),
          'posts_per_page' => -1
        );
        $portfolioDes = new WP_Query($argsPortfolioDes);

        if ($portfolioDes->have_posts()) : while ($portfolioDes->have_posts()) : $portfolioDes->the_post();
        ?> <?php
            $images = get_field('fotos');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)
            if ($images) : ?> <div class="item i--<?= $contadorItems ?>"> <?php foreach ($images as $image) : ?> <?php if ($contador == 1) : ?> <a class="px-0" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"><!-- <img src="<?php echo esc_url($image['sizes']['large']) ?>" alt="" class="img-fluid"> --></div></a> <?php else : ?> <a class="d-none" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"></div></a> <?php endif;
                  $contador++;
                endforeach;
                $contador = 1; ?> </div> <?php endif; ?> <?php
        $contadorItems++;
        if($contadorItems > 5){
            $contadorItems = 1;
        }
          endwhile;
        endif; wp_reset_query();?> <!-- design --></div><div class="tab-pane portfolios fade" id="paisagismo" role="tabpanel" aria-labelledby="paisagismo-tab"> <?php
        $contador = 1;
        $contadorItems = 1;
        $argsPortfolioPai = array(
          'post_type' => 'portfolio',
          'order' => 'ASC',
          'tax_query' => array(
            array(
              'taxonomy' => 'categorias_portfolio',
              'field' => 'slug',
              'terms' => 'paisagismo'
              
            )
        ),
          'posts_per_page' => -1
        );
        $portfolioPai = new WP_Query($argsPortfolioPai);

        if ($portfolioPai->have_posts()) : while ($portfolioPai->have_posts()) : $portfolioPai->the_post();
        ?> <?php
            $images = get_field('fotos');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)
            if ($images) : ?> <div class="item i--<?= $contadorItems ?>"> <?php foreach ($images as $image) : ?> <?php if ($contador == 1) : ?> <a class="px-0" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"><!-- <img src="<?php echo esc_url($image['sizes']['large']) ?>" alt="" class="img-fluid"> --></div></a> <?php else : ?> <a class="d-none" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"></div></a> <?php endif;
                  $contador++;
                endforeach;
                $contador = 1; ?> </div> <?php endif; ?> <?php
        $contadorItems++;
        if($contadorItems > 5){
            $contadorItems = 1;
        }
          endwhile;
        endif; wp_reset_query();?> <!-- paisagismo --></div></div></section><section class="page-projetos d-lg-block d-none portfolios"><div class="row"> <?php 
  $contagem = 1;
  $contador = 1;
  $argsPortfolioArq = array(
          'post_type' => 'portfolio',
          'order' => 'ASC',
          'tax_query' => array(
              array(
                'taxonomy' => 'categorias_portfolio',
                'field' => 'slug',
                'terms' => 'destaque'
                
              )
          ),
          'posts_per_page' => 2
        );
        $portfolioArq = new WP_Query($argsPortfolioArq);

        if ($portfolioArq->have_posts()) : while ($portfolioArq->have_posts()) : $portfolioArq->the_post();
         ?> <?php
            $images = get_field('fotos');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)
            ?> <?php if($contagem == 1):
          if ($images) : ?> <div class="col-6"><div class="item i-d-<?= $contagem ?>"> <?php foreach ($images as $image) : ?> <?php if ($contador == 1) : ?> <a class="px-0" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"><!-- <img src="<?php echo esc_url($image['sizes']['large']) ?>" alt="" class="img-fluid"> --></div></a> <?php else : ?> <a class="d-none" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"></div></a> <?php endif;
    $contador++;
  endforeach;
  $contador = 1; ?> </div></div> <?php endif; ?> <div class="col-6"><div class="cabecalho"><h1 class="titulo">projetos</h1><p>Para o seu projeto residencial, comercial ou corporativo, nós oferecemos atendimento completo: arquitetura, design de interiores e paisagismo, para um resultado personalizado e harmônico.</p><ul class="nav nav-tabs" id="myTab" role="tablist"><li class="nav-item" role="presentation"><a class="nav-link active" id="arquitetura-tab-desk" data-toggle="tab" href="#arquitetura-desk" role="tab" aria-controls="arquitetura-desk" aria-selected="true">arquitetura</a></li><li class="nav-item" role="presentation"><a class="nav-link" id="design-tab-desk" data-toggle="tab" href="#design-desk" role="tab" aria-controls="design-desk" aria-selected="false">design de interiores</a></li><li class="nav-item" role="presentation"><a class="nav-link" id="paisagismo-tab-desk" data-toggle="tab" href="#paisagismo-desk" role="tab" aria-controls="paisagismo-desk" aria-selected="false">paisagismo</a></li></ul></div> <?php endif; ?> <?php if($contagem == 2): 
          if ($images) : ?> <div class="item i-d-<?= $contagem ?>"> <?php foreach ($images as $image) : ?> <?php if ($contador == 1) : ?> <a class="px-0" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"><!-- <img src="<?php echo esc_url($image['sizes']['large']) ?>" alt="" class="img-fluid"> --></div></a> <?php else : ?> <a class="d-none" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"></div></a> <?php endif;
      $contador++;
    endforeach;
    $contador = 1; ?> </div> <?php endif; ?> </div> <?php endif; ?> <?php $contagem++; endwhile; endif; ?> </div><div class="tab-content" id="myTabContentDesk"><div class="tab-pane portfolios fade show active" id="arquitetura-desk" role="tabpanel" aria-labelledby="arquitetura-tab-desk"> <?php
        $contador = 1;
        $contadorItems = 1;
        $argsPortfolioArq = array(
          'post_type' => 'portfolio',
          'order' => 'ASC',
          'tax_query' => array(
              array(
                'taxonomy' => 'categorias_portfolio',
                'field' => 'slug',
                'terms' => 'arquitetura'
                
              )
          ),
          'posts_per_page' => 6
        );
        $portfolioArq = new WP_Query($argsPortfolioArq);

        if ($portfolioArq->have_posts()) : while ($portfolioArq->have_posts()) : $portfolioArq->the_post();
        ?> <?php
            $images = get_field('fotos');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)
            if ($images) : ?> <div class="item i--<?= $contadorItems ?>"> <?php foreach ($images as $image) : ?> <?php if ($contador == 1) : ?> <a class="px-0" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"><!-- <img src="<?php echo esc_url($image['sizes']['large']) ?>" alt="" class="img-fluid"> --></div></a> <?php else : ?> <a class="d-none" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"></div></a> <?php endif;
                  $contador++;
                endforeach;
                $contador = 1; ?> </div> <?php endif; ?> <?php
        $contadorItems++;
        if($contadorItems > 6){
            $contadorItems = 1;
        }
          endwhile;
        endif; wp_reset_query();?> <!-- arquitetura --></div><div class="tab-pane portfolios fade" id="design-desk" role="tabpanel" aria-labelledby="design-tab-desk"> <?php
        $contador = 1;
        $contadorItems = 1;
        $argsPortfolioDes = array(
          'post_type' => 'portfolio',
          'order' => 'ASC',
          'tax_query' => array(
            array(
              'taxonomy' => 'categorias_portfolio',
              'field' => 'slug',
              'terms' => 'design-de-interiores'
              
            )
        ),
          'posts_per_page' => 6
        );
        $portfolioDes = new WP_Query($argsPortfolioDes);

        if ($portfolioDes->have_posts()) : while ($portfolioDes->have_posts()) : $portfolioDes->the_post();
        ?> <?php
            $images = get_field('fotos');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)
            if ($images) : ?> <div class="item i--<?= $contadorItems ?>"> <?php foreach ($images as $image) : ?> <?php if ($contador == 1) : ?> <a class="px-0" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"><!-- <img src="<?php echo esc_url($image['sizes']['large']) ?>" alt="" class="img-fluid"> --></div></a> <?php else : ?> <a class="d-none" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"></div></a> <?php endif;
                  $contador++;
                endforeach;
                $contador = 1; ?> </div> <?php endif; ?> <?php
        $contadorItems++;
        if($contadorItems > 5){
            $contadorItems = 1;
        }
          endwhile;
        endif; wp_reset_query();?> <!-- design --></div><div class="tab-pane portfolios fade" id="paisagismo-desk" role="tabpanel" aria-labelledby="paisagismo-tab-desk"> <?php
        $contador = 1;
        $contadorItems = 1;
        $argsPortfolioPai = array(
          'post_type' => 'portfolio',
          'order' => 'ASC',
          'tax_query' => array(
            array(
              'taxonomy' => 'categorias_portfolio',
              'field' => 'slug',
              'terms' => 'paisagismo'
              
            )
        ),
          'posts_per_page' => 6
        );
        $portfolioPai = new WP_Query($argsPortfolioPai);

        if ($portfolioPai->have_posts()) : while ($portfolioPai->have_posts()) : $portfolioPai->the_post();
        ?> <?php
            $images = get_field('fotos');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)
            if ($images) : ?> <div class="item i--<?= $contadorItems ?>"> <?php foreach ($images as $image) : ?> <?php if ($contador == 1) : ?> <a class="px-0" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"><!-- <img src="<?php echo esc_url($image['sizes']['large']) ?>" alt="" class="img-fluid"> --></div></a> <?php else : ?> <a class="d-none" href="<?php echo $image['url'] ?>" data-lightbox="<?= $post->post_name; ?>" data-title="<?php echo $image['caption'] ?>"><div class="item-img" style="background-image: url( <?php echo esc_url($image['sizes']['large']) ?>);"></div></a> <?php endif;
                  $contador++;
                endforeach;
                $contador = 1; ?> </div> <?php endif; ?> <?php
        $contadorItems++;
        if($contadorItems > 5){
            $contadorItems = 1;
        }
          endwhile;
        endif; wp_reset_query();?> <!-- paisagismo --></div></div></section> <?= get_template_part('clientes'); ?> <?= get_template_part('contato'); ?> <?php get_footer(); ?>
<footer><div class="container d-lg-flex"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Logo-white.png" alt=""><div class="midias d-lg-none"><a href="#"><i class="fab fa-facebook-square"></i></a> <a href="#"><i class="fab fa-instagram-square"></i></a></div><div class="content col-lg-7 px-0"><div class="box"><span class="box-title">lorem ipsum</span> <a href="#" class="sub wobble-vertical">lorem ipsum</a> <a href="#" class="sub wobble-vertical">lorem ipsum</a> <a href="#" class="sub wobble-vertical">lorem ipsum</a></div><div class="box"><span class="box-title">lorem ipsum</span> <a href="#" class="sub wobble-vertical">lorem ipsum</a> <a href="#" class="sub wobble-vertical">lorem ipsum</a> <a href="#" class="sub wobble-vertical">lorem ipsum</a></div><div class="box"><span class="box-title">lorem ipsum</span> <a href="#" class="sub wobble-vertical">lorem ipsum</a> <a href="#" class="sub wobble-vertical">lorem ipsum</a> <a href="#" class="sub wobble-vertical">lorem ipsum</a></div></div><div class="midias d-none d-lg-block col-lg-2 px-0"><a href="#"><i class="fab fa-facebook-square wobble-vertical"></i></a> <a href="#"><i class="fab fa-instagram-square wobble-vertical"></i></a></div></div><div class="dev-by"><span>2020 - Todos os direitos reservados à Maho Arquitetura.</span> <span>Desenvolvido por <a href="https://humann.com.br" class="wobble-vertical" target="_blank"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-humann.png" alt=""></a></span></div></footer> <?php wp_footer(); ?> <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script><script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.3/jquery.inputmask.min.js"></script><script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script><script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script><script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/lightbox.min.js"></script><script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script><!-- <script src="//tag.goadopt.io/injector.js?website_code=a264db25-e5a9-4fdb-b150-38d3364b5830" class="adopt-injector"></script> --><script>AOS.init();

  $(document).ready(function(){ <?php if(is_page(array(6,8))):?> var sliderClientes = tns({
    container: ".carosel-clientes",
    items: 2.3,
    slideBy: 1,
    autoplay: false,
    loop: false,
    touch: true,
    gutter: 10,
    autoplayButtonOutput: false,
    controls: false,
    controlsContainer: '.carosel-control',
    nav: false,
    responsive: {
      600: {
        items: 2.3,
      },
      1000: {
        items: 4,
        controls:true,
        
      },
    },
  }); <?php endif; if(is_page( array(6,48) )): ?> var sliderSolucao = tns({
    container: ".carousel-solucoes",
    items: 1.1,
    slideBy: 1,
    autoplay: false,
    loop: false,
    touch: true,
    autoplayButtonOutput: false,
    controls: false,
    nav: false,
    responsive: {
      600: {
        items: 2.1,
      },
      1000: {
        disable: true,
      },
    },
  }); <?php endif; ?> })</script>
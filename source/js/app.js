$(".menu-icon").on("click", function () {
  $(".top-nav").toggleClass("ativo");
});

$(".btn-contato").on("click", function (e) {
  e.preventDefault();
  $("html, body").animate(
    {
      scrollTop: $("#contato").offset().top - 120,
    },
    500
  );
  $(".top-nav").removeClass("ativo");
  $(".menu-btn")[0].checked = false;
});

$(function () {
  $(document).on("scroll", function () {
    const scroll = $("html")[0].scrollTop;

    if (scroll > 100) {
      $(".menu-home").addClass("red-class");
    } else {
      $(".menu-home").removeClass("red-class");
    }
  });
});

function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}

$(".e-mail").attr("onChange", "validateEmail(this)");

$(".phone").inputmask({
  placeholder: "",
  mask: function () {
    return ["(99) 9999-9999", "(99) 99999-9999"];
  },
});



let responseForm = $(".wpcf7-response-output")[0].attributes[1].value;

$(".wpcf7-submit").on("click", function () {
  console.log(responseForm);

  setTimeout(function () {
    if (responseForm) {
      $(".yellow-lg ").attr("style", "height:480px ");
    } else {
      $(".yellow-lg ").attr("style", "height:430px ");
    }
  }, 300);
});

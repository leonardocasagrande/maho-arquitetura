<div class="clientes">
    <div class="conter-carosel">
        <div class="titulo">
        <h2 class="">nossos clientes</h2>
        <div class="carosel-control">
            <img src="<?= get_stylesheet_directory_uri(  ) ?>/dist/img/left.png" alt="">
            <img src="<?= get_stylesheet_directory_uri(  ) ?>/dist/img/right.png" alt="">
        </div>
        </div>
        <div class="col-carosel">
        <div class="carosel-clientes">
        <?php 
            $argsClientes = array(
                'post_type' => 'clientes',
                'order' => 'ASC',
                'posts_per_page' => -1
              );
              $clientes = new WP_Query($argsClientes);
      
              if ($clientes->have_posts()) : while ($clientes->have_posts()) : $clientes->the_post();
        ?>
            <div class="item">
                <img src="<?= get_the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
            </div>
        <?php endwhile; endif; ?>
        </div>
        </div>
    </div>
</div>
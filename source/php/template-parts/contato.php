<section class="contato" id="contato">

  <div class="container">

    <div class="col-lg-3 px-0">
      <span class="title col-6 col-md-4 col-lg-12 px-0">entre em contato</span>

      <div class="infos">

        <a class="icon" href="mailto:contato@mahoarquitetura.com.br">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/plane-icon.png" alt="">
          <span>contato@mahoarquitetura.com.br</span>
        </a>

        <div class="icon">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/tel-icon.png" alt="">
          <a href="tel:+551933363336">19 3333-3336</a>
          <span class="px-2">|</span>
          <a href="tel:5555">11 99999-3399</a>
        </div>

        <a class="icon" href="#">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/map-icon.png" alt="">
          <span>Rua Cláudio Abreu, 50, Sala 4 Cambuí, Campinas/SP</span>
        </a>

      </div>

      <div class="form">

        <?= do_shortcode('[contact-form-7 id="5" title="contato"]'); ?>


      </div>
    </div>
  </div>

  <a class="d-md-none" href="" target="_blank"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/map-mob.png" alt=""></a>

  <a class="d-none d-md-block lg-img " href="" target="_blank">
    <div></div>
  </a>

</section>
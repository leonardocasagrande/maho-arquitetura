<section class="nossas-solucoes">
  <div class="container">

    <div class="col-lg-2 px-0 pt-lg-4">
      <span class="title col-md-4 col-lg-12 px-0">nossas soluções</span>

      <p class="lg-size">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec scelerisque eros at quam pellentesque, eu ullamcorper velit egestas. Nulla rutrum id massa sit amet </p>
    </div>

    <div class="wrapper col-lg-10 px-0">
      <div class="carousel-solucoes d-lg-flex justify-content-end">

        <div class="item ">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/solucao-1.png" alt="">

          <span class="name">Arquitetura</span>

          <p>Projetos personalizados, arrojados e completos para dar forma ao seu sonho, seja ele residencial, comercial ou corporativo, atendendo aos seus desejos e necessidades com o devido respeito e compreensão.</p>

        </div>

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/solucao-2.png" alt="">

          <span class="name">design de interiores</span>

          <p>Projetos personalizados, arrojados e completos para dar forma ao seu sonho, seja ele residencial, comercial ou corporativo, atendendo aos seus desejos e necessidades com o devido respeito e compreensão.</p>

        </div>

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/solucao-3.png" alt="">

          <span class="name">paisagismo</span>

          <p>Projetos personalizados, arrojados e completos para dar forma ao seu sonho, seja ele residencial, comercial ou corporativo, atendendo aos seus desejos e necessidades com o devido respeito e compreensão.</p>

        </div>

      </div>
    </div>

  </div>
</section>
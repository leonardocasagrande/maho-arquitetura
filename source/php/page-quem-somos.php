<?= get_header(); ?>

<section class="quem-somos">

  <div class="container">

    <div class="img-lg-1 col-lg-5 px-0 d-none d-lg-block"></div>


    <div class="col-lg-7 px-0 wrapper">

      <div class="col-lg-10 px-0 text">
        <span class="title">quem somos</span>

        <p>Nós viemos desafiar padrões. Arquitetura é lugar para inovação. A casa dos seus sonhos é sua, e essa ideia está presente em cada detalhe dos nossos projetos, porque acreditamos que, além de linda, confortável e acolhedora, ela pode e deve ser uma expressão de quem você é.</p>

        <p>Nós viemos derrubar mitos. Dar vida ao seu sonho é acessível, quando há criatividade, respeito e uma profunda compreensão dos seus desejos e necessidades. Por isso, queremos conhecê-los, os sonhos e o sonhadores.</p>

        <p>Nós somos Adria e Mariana. Bem vindos à Maho Arquitetura.</p>
      </div>

      <img class="col-lg-12 px-0 teste d-none d-lg-block " src="<?= get_stylesheet_directory_uri(); ?>/dist/img/quem-somos-lg-2.png" alt="">

      <div class="d-none teste d-lg-flex ">
        <img class="col-lg-6  mb-0 px-0" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/quem-somos-lg-3.png" alt="">
        <img class="col-lg-6 px-0 mb-0" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/quem-somos-lg-4.png" alt="">
      </div>

    </div>
  </div>

  <div class="d-lg-none md-align">
    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/quem-somos-mob-1.png" alt="">

    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/quem-somos-mob-2.png" alt="">
  </div>

</section>

<section class="personas">

  <div class="container">

    <div class="item">
      <img class=" col-md-6" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/black-square.png" alt="">


      <div class=" col-lg-6 text ml-lg-5">
        <span class="title col-lg-6 px-0">mariana fedocci</span>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sodales tristique maximus. Etiam nisi lacus, suscipit non blandit et, ullamcorper nec nibh. Cras cursus nibh sit amet aliquam convallis. </p>
        <p>Mauris ullamcorper lectus a risus pretium, non tincidunt odio luctus. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>

        <a href="#"><i class="fab fa-facebook-square"></i></a>
        <a href="#"><i class="fab fa-instagram-square"></i></a>
      </div>
    </div>

    <div class="item">
      <img class=" col-md-6" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/black-square.png" alt="">

      <div class=" col-lg-6 text mr-lg-5">
        <span class="title col-lg-6 px-0">adria pieroni</span>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sodales tristique maximus. Etiam nisi lacus, suscipit non blandit et, ullamcorper nec nibh. Cras cursus nibh sit amet aliquam convallis. </p>
        <p>Mauris ullamcorper lectus a risus pretium, non tincidunt odio luctus. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>

        <a href="#"><i class="fab fa-facebook-square"></i></a>
        <a href="#"><i class="fab fa-instagram-square"></i></a>

      </div>

    </div>

</section>

<?= get_template_part('nossas-solucoes'); ?>

<?= get_template_part('contato'); ?>

<?= get_footer(); ?>
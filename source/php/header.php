<!DOCTYPE html>

<html lang="pt_BR">

<head>



  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title> <?= wp_title(); ?></title>

  <meta name="robots" content="index, follow" />

  <meta name="msapplication-TileColor" content="#ffffff">

  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>

  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <script src="https://kit.fontawesome.com/feb691859e.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">

  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/lightbox.min.css">


  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">

</head>

<body>


  <?php if (is_home()) : $home = ' menu-home';
  endif; ?>

  <header class="d-lg-block d-none <?= $home; ?> ">


    <div class="header ">
      <div class="container">

        <a href="<?= get_site_url(); ?>/">
          <img class="logo-header" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Logo-white.png" alt="">
        </a>

        <div class="links  px-0">

          <a class="wobble-vertical" href="<?= get_site_url(); ?>/quem-somos">quem somos</a>
          <a class="wobble-vertical" href="#">projetos</a>
          <a class="btn-contato wobble-vertical" href="#contato">contato</a>

        </div>


        <div class="midias-header">
          <a href="#"><i class="fab fa-facebook-square wobble-vertical"></i></a>
          <a href="#"><i class="fab fa-instagram-square wobble-vertical"></i></a>
        </div>

      </div>
    </div>

  </header>







  <nav class="<?= $home; ?>">
    <div class=" d-lg-none top-nav   " id="top-nav">

      <a href="<?= get_site_url(); ?>">

        <img class="logo-header" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Logo-white.png" alt="goma">

        <img class="logo-header-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Logo.png" alt="goma">
      </a>

      <input class="menu-btn" type="checkbox" id="menu-btn" />

      <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>

      <div class="menu">

        <a href="<?= get_site_url(); ?>/quem-somos">quem somos</a>
        <a href="#">projetos</a>
        <a class="btn-contato" href="#contato">contato</a>

      </div>

      <div class="midias-header">
        <a href="#"><i class="fab fa-facebook-square"></i></a>
        <a href="#"><i class="fab fa-instagram-square"></i></a>
      </div>

    </div>
  </nav>
  <div class="header-mask"></div>
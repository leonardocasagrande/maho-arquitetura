<?php


function my_function_admin_bar()
{
	return false;
}
add_filter('show_admin_bar', 'my_function_admin_bar');

add_theme_support( 'post-thumbnails' );

function cptui_register_my_taxes_categorias_portfolio() {

	/**
	 * Taxonomy: Categorias.
	 */

	$labels = [
		"name" => __( "Categorias", "custom-post-type-ui" ),
		"singular_name" => __( "Categoria", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Categorias", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'categorias_portfolio', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "categorias_portfolio",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => true,
			];
	register_taxonomy( "categorias_portfolio", [ "portfolio" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes_categorias_portfolio' );

function cptui_register_my_cpts() {

	/**
	 * Post Type: Portfólio.
	 */

	$labels = [
		"name" => __( "Portfólio", "custom-post-type-ui" ),
		"singular_name" => __( "Portfólio", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Portfólio", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "portfolio", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail" ],
	];

	register_post_type( "portfolio", $args );
	

		/**
		 * Post Type: Clientes.
		 */
	
		$labels = [
			"name" => __( "Clientes", "custom-post-type-ui" ),
			"singular_name" => __( "Cliente", "custom-post-type-ui" ),
		];
	
		$args = [
			"label" => __( "Clientes", "custom-post-type-ui" ),
			"labels" => $labels,
			"description" => "",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"show_in_rest" => true,
			"rest_base" => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive" => false,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"delete_with_user" => false,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => [ "slug" => "clientes", "with_front" => true ],
			"query_var" => true,
			"supports" => [ "title", "thumbnail" ],
		];
	
		register_post_type( "clientes", $args );
	
	
}

add_action( 'init', 'cptui_register_my_cpts' );

function formatPhone($phone)
{
    $formatedPhone = preg_replace('/[^0-9]/', '', $phone);
    $matches = [];
    preg_match('/^([0-9]{2})([0-9]{4,5})([0-9]{4})$/', $formatedPhone, $matches);
    if ($matches) {
        return '('.$matches[1].') '.$matches[2].'-'.$matches[3];
    }

    return $phone; // return number without format
}
?>